import numpy as np
import cv2
import os
import imageio
from abc import ABC, abstractmethod


class RGBWeights(ABC):

    @abstractmethod
    def get_rgb_weights(self, wavelength):
        pass


## Implemented two functions for rgb weights.

class RGBWeightsViaCieXYZ(RGBWeights):

    # Wavelength -> CIE XYZ -> RGB.
    # Idea from https://stackoverflow.com/questions/3407942/rgb-values-of-visible-spectrum
    def get_rgb_weights(self, wavelength):
        # Load CIE color matching function values into a dict.
        cie = self.__load_cie_values()

        # Get the XYZ that match the wavelength.
        XYZ = cie.get(str(wavelength))

        # Convert XYZ to the the rgb values that.
        return self.__convert_XYZ_to_rgb(XYZ)

    # Load CIE color matching function values into a dictionary -> XYZ values that correspond to wavelengths.
    def __load_cie_values(self):
        cie = dict()

        # This file contains the XYZ values that correspond to the wavelengths.
        # https://scipython.com/static/media/blog/colours/cie-cmf.txt
        with open(os.path.join('data', 'cie-cmf.txt')) as f:
            for line in f:
                data = line.strip().split()

                # Key = wavelength; Value = XYZ
                cie[data[0]] = [data[1], data[2], data[3]]

        return cie

    # Conversion formulas.
    # https://stackoverflow.com/questions/43494018/converting-xyz-color-to-rgb
    def __convert_XYZ_to_rgb(self, XYZ):
        X = float(XYZ[0])
        Y = float(XYZ[1])
        Z = float(XYZ[2])

        # Linear relationship between RGB and XYZ spaces -> resulting values are clipped to the 0 to 1 range.
        r = np.clip(3.2404542 * X - 1.5371385 * Y - 0.4985314 * Z, 0, 1)
        g = np.clip(-0.9692660 * X + 1.8760108 * Y + 0.0415560 * Z, 0, 1)
        b = np.clip(0.0556434 * X - 0.2040259 * Y + 1.0572252 * Z, 0, 1)

        # Additional gamma correction for sRGB.
        return self.__gamma_correction(r), self.__gamma_correction(g), self.__gamma_correction(b)

    def __gamma_correction(self, linear_value):
        if abs(linear_value) < 0.0031308:
            return 12.92 * linear_value
        else:
            return 1.055 * pow(linear_value, 0.41666) - 0.055;


class RGBWeightsViaVisibleSpectrum(RGBWeights):

    # Weights formula adapted from https://stackoverflow.com/questions/3407942/rgb-values-of-visible-spectrum
    def get_rgb_weights(self, wavelength):
        if (wavelength >= 380) and (wavelength < 440):
            r = -(wavelength - 440.) / (440. - 380.)
            g = 0.0
            b = 1.0
        elif (wavelength >= 440) and (wavelength < 490):
            r = 0.0
            g = (wavelength - 440.) / (490. - 440.)
            b = 1.0
        elif (wavelength >= 490) and (wavelength < 510):
            r = 0.0
            g = 1.0
            b = -(wavelength - 510.) / (510. - 490.)
        elif (wavelength >= 510) and (wavelength < 580):
            r = (wavelength - 510.) / (580. - 510.)
            g = 1.0
            b = 0.0
        elif (wavelength >= 580) and (wavelength < 645):
            r = 1.0
            g = -(wavelength - 645.) / (645. - 580.)
            b = 0.0
        elif (wavelength >= 645) and (wavelength <= 780):
            r = 1.0
            g = 0.0
            b = 0.0
        else:
            r = 0.0
            g = 0.0
            b = 0.0

        return r, g, b


class Application:

    def __init__(self, data_path):
        # The spectral reflectance data ranges from 400nm to 700nm.
        self.wavelengths = [l for l in range(400, 710, 10)]
        # Load the multispectral images that correspond to the wavelengths from 400nm to 700nm.
        self.bands = self.__load_images(data_path)

    def __load_images(self, data_path):
        images = list()

        for file in os.listdir(data_path):
            # Originally 16-bit image.
            image = cv2.imread(os.path.join(data_path, file), cv2.IMREAD_UNCHANGED)

            # Convert to 8-bit image -> reference image is 8-bit.
            image = cv2.convertScaleAbs(image, alpha=(255.0 / 65535.0))

            images.append(image)

        return np.array(images)

    def __normalize(self, image):
        min = np.min(image, axis=(0, 1), keepdims=True)
        max = np.max(image, axis=(0, 1), keepdims=True)
        return (image - min) / (max - min)

    def create_rgb_image(self, rgb_weights_formula: RGBWeights):

        # Create empty arrays for each color channel.
        r_channel = np.zeros((self.bands.shape[1], self.bands.shape[2]))
        g_channel = np.zeros((self.bands.shape[1], self.bands.shape[2]))
        b_channel = np.zeros((self.bands.shape[1], self.bands.shape[2]))

        bands_copy = self.bands.copy()

        for index, wl in enumerate(self.wavelengths):
            # RGB weights inferred from the corresponding wavelength.
            r_weight, g_weight, b_weight = rgb_weights_formula.get_rgb_weights(wl)

            # Multiply each weight with the band that matches the wavelength.
            r_channel += bands_copy[index] * r_weight
            g_channel += bands_copy[index] * g_weight
            b_channel += bands_copy[index] * b_weight

        # Combine the color channels.
        image_rgb = np.dstack((r_channel, g_channel, b_channel))

        # Normalize the pixel values.
        image_rgb = self.__normalize(image_rgb) * 255

        # Save as uint8 and return the image.
        return image_rgb.astype(np.uint8)


IMAGES_PATH = os.path.join('data', 'multispectral_images')
app = Application(IMAGES_PATH)

rgb_image_via_cie_xyz = app.create_rgb_image(RGBWeightsViaCieXYZ())
imageio.imwrite('test_result_via_cie_xyz.png', rgb_image_via_cie_xyz)

rgb_image_via_visible_spectrum = app.create_rgb_image(RGBWeightsViaVisibleSpectrum())
imageio.imwrite('test_result_via_visible_spectrum.png', rgb_image_via_visible_spectrum)
