## Setting Project Up

1. Create your virtual environment with the dependencies in the yaml file
```
$ conda env create -f environment.yml
```
2. Activate environment   
```
$ conda activate multispectral
```
3. Go to the project folder and run the main.py file to create the images 
```
$ python main.py
```
